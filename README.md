# lease-project

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### init

```sh
npm init/create vue@3
```

### 大版本.次要版本.小版本

波浪号（tilde）+指定版本：比如~1.2.2，表示安装1.2.x的最新版本 安装时不改变大版本号和次要版本号
插入号（caret）+指定版本：比如ˆ1.2.2，表示安装1.x.x的最新版本 安装时不改变大版本号
    注意，如果大版本号为0，则插入号的行为与波浪号相同，这是因为此时处于开发阶段，即使是次要版本号变动，也可能带来程序的不兼容
latest：安装最新版本

### 正则

```sh
​i: ignoreCase, 匹配忽视大小写
m: multiline , 多行匹配
g: global , 全局匹配
```

### 初始化 vue2 项目 (使用 vite -> vue2.7.x: 支持组合式api)

```shell
npm init vue@2
npm i element-ui -S
npm i axios --save
npm i vue-axios --save
```
