import request from '@/common/request'

let leaseManage = '/v1/leaseManage'
let vehicleManage = '/v1/vehicleManage'

// [车辆类型]
// 新增类型
export const createType = (data) => request.post(vehicleManage + '/type', data)
// 更新类型
export const editType = (data) => request.put(vehicleManage + '/type', data)
// 删除类型 data: 整个类型对象
export const deleteType = (data) => request.delete(vehicleManage + '/type', data)
// 获取父类型的子类型列表 (不传值默认获取顶层)
export const queryTypeTree = (data) => request.get(vehicleManage + `/type/list/${data.parent}`)
// *获取单个类型
export const getType = (data) => request.get(vehicleManage + `/type/${data.id}`)

// [车辆]
// 车辆分页 page limit type 车辆类型  非必填: [carName] [license]车牌号
export const queryCarPage = (data) => request.get(vehicleManage + '/car/page', { params: data })
// 新增车辆
export const createCar = (data) => request.post(vehicleManage + '/car', data)
// 编辑车辆
export const editCar = (data) => request.put(vehicleManage + '/car', data)
// 删除车辆 data: list[int] ids
export const deleteCar = (data) => request.delete(vehicleManage + '/car', data)
// *获取特定车辆类型的汽车列表 (state: 1/0汽车租赁状态, 可以为空)
export const queryCarList = (data) => request.get(vehicleManage + `/car/list/${data.type}/${data.state}`)

// [订单]
// 查询租赁订单分页 4: page limit oid(订单号 string) cid(车辆id int)
export const queryOrderPage = (data) => request.get(leaseManage + '/order/page', { params: data })
// 创建租赁订单
export const createOrder = (data) => request.post(leaseManage + '/order', data)
// 删除租赁订单 data: list[int] ids
export const deleteOrder = (data) => request.delete(leaseManage + '/order', data)
