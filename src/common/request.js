import axios from 'axios'
import { Message } from 'element-ui'

let baseUrl = '/api'

const service = axios.create({
  baseURL: baseUrl,
  timeout: 5000
})

// 响应拦截
service.interceptors.response.use(
  (response) => {
    const res = response.data
    if (res.code !== 200 && res.code !== '200') {
      Message({
        message: res.message || res.msg || '与服务器的连接可能中断，请尝试重新连接',
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(new Error(res.message || '与服务器的连接可能中断，请尝试重新连接'))
    } else {
      return res
    }
  },
  (error) => {
    console.log('err' + error)
    Message({
      message: '与服务器的连接可能中断，请尝试重新连接',
      type: 'error',
      showClose: true,
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
